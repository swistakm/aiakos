from django.apps import AppConfig


class DjangoProfileOidcConfig(AppConfig):
	name = 'django_profile_oidc'
	verbose_name = 'User profiles'
